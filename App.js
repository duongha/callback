import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Dangky from './Component/Dangky';
import HomeScreen from './Component/HomeScreen';
import Details from './Component/Details';
import 'react-native-gesture-handler';

class App extends Component {
  render() {
    const Stack = createStackNavigator();
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            options={{headerShown: false}}
            name="Home"
            component={HomeScreen}
          />
          <Stack.Screen name="Details" component={Details} />
          {/* <Stack.Screen name="Dangky" component={Dangky} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;
