import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

class Dangky extends Component {
  constructor(props) {
    debugger;
    super(props);
    this.state = {
      vl: this.props.route.params.vl,
    };
  }
  _handleCount = (a) => {
    this.setState({
      vl: this.state.vl + 1,
    });
  };
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>{this.state.vl}</Text>
        <TouchableOpacity
          style={{
            width: 220,
            height: 40,
            backgroundColor: 'green',
            borderWidth: 1,
            borderColor: 'white',
            borderRadius: 50,
            margin: 15,
          }}
          onPress={() => {
            this.props.route.params.bbb();
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 18,
              textAlign: 'center',
              padding: 7,
              fontWeight: '600',
            }}>
            CallBack
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Dangky;
