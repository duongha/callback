import React, {Component} from 'react';
import {View, TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataNavigate: {...this.props.route.params.ha},
    };
  }

  render() {
    const {dataNavigate} = this.state;
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <TextInput
          style={{
            height: 40,
            width: 160,
            marginBottom: 10,
            borderColor: 'gray',
            borderWidth: 1,
          }}
          value={dataNavigate.ten}
          onChangeText={(value) => {
            dataNavigate.ten = value;
            this.setState({dataNavigate: this.state.dataNavigate});
          }}
        />
        <TextInput
          style={{
            height: 40,
            width: 160,
            marginBottom: 10,
            borderColor: 'gray',
            borderWidth: 1,
          }}
          onChangeText={(value) => {
            dataNavigate.tuoi = value;
            this.setState({dataNavigate: this.state.dataNavigate});
          }}
          value={dataNavigate.tuoi}
        />

        <TouchableOpacity
          style={{
            width: 220,
            height: 40,
            backgroundColor: 'green',
            borderWidth: 1,
            borderColor: 'white',
            borderRadius: 50,
            margin: 15,
          }}
          onPress={() => {
            this.props.route.params.aaa(dataNavigate);
          }}
        />
      </View>
    );
  }
}

export default Details;
