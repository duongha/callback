import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {} from 'react-native-gesture-handler';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ha: {
        ten: 'aaaa',
        tuoi: '22',
      },
    };
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>{this.state.ha.ten}</Text>
        <Text>{this.state.ha.tuoi}</Text>

        <TouchableOpacity
          style={{
            width: 220,
            height: 40,
            backgroundColor: 'green',
            borderWidth: 1,
            borderColor: 'white',
            borderRadius: 50,
            margin: 15,
          }}
          onPress={() => {
            this.props.navigation.navigate('Details', {
              ha: this.state.ha,
              aaa: (value) => {
                console.log('value: ', value);
                this.setState({
                  ha: value,
                });
              },
            });
          }}>
          <Text
            style={{
              color: 'white',
              fontSize: 18,
              textAlign: 'center',
              padding: 7,
              fontWeight: '600',
            }}>
            Profile
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default HomeScreen;
